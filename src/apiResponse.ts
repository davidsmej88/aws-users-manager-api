const headers = {
  "content-type": "application/json",
};

export class OkResponse {
  constructor(statusCode: number, body: string) {
    this.statusCode = statusCode;
    this.body = body;
    this.headers = headers;
  }
  statusCode: number;
  body: string;
  headers: object;
}