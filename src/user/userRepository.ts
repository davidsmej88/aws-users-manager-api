import AWS from "aws-sdk";
import { v4 } from "uuid";
import { User } from "./userDto";
import { HttpError } from "../errorHandler";

export class UserRepository {
  private docClient = new AWS.DynamoDB.DocumentClient();
  readonly tableName = 'UsersTable';

  async saveUser(userCommand: User) {
    const user = {
      ...userCommand,
      userID: v4(),
    }
    await this.docClient.put({
      TableName: this.tableName,
      Item: user,
    }).promise();

    return user;
  }

  async getUserById(id: string | undefined) {
    const user = await this.docClient.get({
      TableName: this.tableName,
      Key: {
        userID: id,
      }
    }).promise();

    if (!user?.Item) {
      return new HttpError(
        404,
        JSON.stringify(
          {
            error: `User with id ${id} not found`
          }
        )
      );
    }

    return user.Item;
  }
}