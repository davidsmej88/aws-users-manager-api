import { APIGatewayProxyEvent } from "aws-lambda";
import { handleError } from "../errorHandler";
import { OkResponse } from "../apiResponse";
import { UserRepository } from "./userRepository";
import { userSchema } from "./userDto";

const userRepository = new UserRepository();

export const createUser = async (event: APIGatewayProxyEvent): Promise<OkResponse> => {
  try {
    const reqBody = JSON.parse(event.body as string);
    await userSchema.validate(reqBody, { abortEarly: false });

    const user = await userRepository.saveUser(reqBody);

    return new OkResponse(201, JSON.stringify(user));
  } catch (e) {
    return handleError(e);
  }
};

export const getUser = async (event: APIGatewayProxyEvent): Promise<OkResponse> => {
  try {
    const id = event.pathParameters?.id;
    const user = await userRepository.getUserById(id);

    return new OkResponse(200, JSON.stringify(user));
  } catch (e) {
    return handleError(e);
  }
};
